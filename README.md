# myConfigAndDotFiles

some of my configuration files

stucture:
-dotfiles
    - vimrc
    - config
    - i3
    - zshrc
-etc files
    -environment

## Vim
install pathogen:
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

vim plugins:
- YouCompleteMe (https://github.com/Valloric/YouCompleteMe)
- vim eunuch (https://github.com/tpope/vim-eunuch)
- nerdtree (https://github.com/scrooloose/nerdtree)
- gitGutter (https://github.com/airblade/vim-gitgutter)
- rust.vim (https://github.com/rust-lang/rust.vim)
- syntastic (https://github.com/vim-syntastic/syntastic)
- airline (https://github.com/vim-airline/vim-airline)
-ctrlp (https://github.com/kien/ctrlp.vim)
(- fzf (https://github.com/junegunn/fzf.vim), first install https://github.com/junegunn/fzf)
[- vim Hardmode (https://github.com/dusans/vim-hardmode)]

Maybe this one when needed(https://github.com/editorconfig/editorconfig-vim) for working with multiple projects.


some i3 specific stuff:
problem with loosing the focus in intellij
https://www.reddit.com/r/i3wm/comments/a4lx84/lost_focus_on_intellij_idea_when_i_change/

```
Help -> Edit Custom Properties
Add suppress.focus.stealing=false.
```
