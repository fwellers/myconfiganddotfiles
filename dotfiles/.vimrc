" based on: 
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2017 Sep 20
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" my tweaked vimrc file

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
  packadd! matchit
endif

" Set vim to vim-only mode
set nocompatible

" Basic configuration
syntax enable
filetype plugin on

" Set path for basic fuzzy search
set path+=**

" Set nummerartion of lines
set nu
set relativenumber

" Set maximum widht of editor
set colorcolumn=110
highlight ColorColumn ctermbg=darkgray


" Set tab indenting
set tabstop=2
set shiftwidth=2
set softtabstop=0
set noexpandtab

" Basic security funtions
set exrc
set secure

" Use pathogen for plugins
execute pathogen#infect()

" #############################################
" ############### Plugins #####################
"
" you-complete-me configuration
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
let g:ycm_always_populate_location_list = 1
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_rust_src_path = '/home/finn/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/'


" Apply YCM FixIt
map <F9> :YcmCompleter FixIt<CR>
" map <C-b> :YcmCompleter GoTo<CR>
nnoremap ,g :YcmCompleter GoTo<CR>
nnoremap ,gi :YcmCompleter GoToImplementation<CR>
nnoremap ,gd :YcmCompleter GoToDeclaration<CR>
nnoremap ,gt :YcmCompleter GetType<CR>
nnoremap <C-b>] :YcmCompleter GoTo<CR>




" nerdtree configuration
map <C-o> :NERDTreeToggle<CR>

" fzf
set rtp+=~/gitStuff/fzf/
map <C-p> :Files<CR>
"let g:fzf_buffers_jump = 1


" airline
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'


set encoding=utf-8
set magic


" If you prefer the Omni-Completion tip window to close when a selection is
" " made, these lines close it on movement in insert mode or when leaving
" " insert mode
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif


" syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set splitright

